---
- name: "Create vhost for nextcloud"
  include_role:
    name: httpd
    tasks_from: vhost
  vars:
    use_letsencrypt: True
    force_tls: True
    redirects:
      - src: "^/index.php$"
        target: /nextcloud/
        match: True

- name: Install packages
  package:
    name:
      - nextcloud-httpd
      - nextcloud-postgresql
      - nextcloud
      - apg
      - php-process #missing dep, remove once package nextcloud is fixed

- name: Get previous database credentials
  block:
    - name: Parse previously generated database password
      shell: "php -r 'include(\"/etc/nextcloud/config.php\"); echo $CONFIG[\"dbpassword\"];'"
      register: res1
      changed_when: False
      no_log: True
    - name: Define password
      set_fact:
        _db_pw: "{{ res1.stdout }}"
      no_log: True
    # as tempting as it is to merge the set_fact actions, it does not work
    - name: Define if database configuration is needed
      set_fact:
        _need_config: "{{ _db_pw == '' or _db_pw == 'rt_pass' }}"

- name: Generate database credentials
  block:
    - name: Generate a password
      command: "apg -m 16 -x 32 -n 1 -M ncl"
      register: res1
      no_log: True

    - name: Define database password
      set_fact:
        _db_pw: "{{ res1.stdout }}"
      no_log: True
  when: _need_config

- block:
    - name: nextcloud DB user
      postgresql_user:
        name: "{{ db_user }}"
        password: "{{ _db_pw }}"
    - name: nextcloud DB
      postgresql_db:
        name: "{{ db_name }}"
        owner: "{{ db_user }}"
        encoding: "UTF-8"
  become: true
  become_user: postgres

- name: Do nextcloud-y stuff
  become: true
  become_user: apache
  block:
    - name: Install Nextcloud
      command: >
        /usr/bin/php ./occ maintenance:install
        --database=pgsql
        --database-name={{ db_name }}
        --database-user={{ db_user }}
        --database-pass={{ _db_pw }}
        --data-dir=/var/lib/nextcloud/data/
        --admin-pass={{ admin_pass }}
        --admin-email={{ admin_email }}
      args:
        chdir: /usr/share/nextcloud
        creates: /var/lib/nextcloud/data/admin/

    - name: Set trusted domain
      command: >
        /usr/bin/php ./occ config:system:set trusted_domains 1 --value={{ website_domain }}
      args:
        chdir: /usr/share/nextcloud

    - name: Check installed applications
      command: >
        /usr/bin/php ./occ app:list --output=json
      args:
        chdir: /usr/share/nextcloud
      register: occ_list
      changed_when: False

    - name: Install applications
      command: >
        /usr/bin/php ./occ app:install {{ item }}
      args:
        chdir: /usr/share/nextcloud
      when: item not in (occ_list.stdout|from_json)['enabled']
      with_items: "{{ applications }}"

- name: Open nextcloud
  file:
    src: /etc/httpd/conf.d/nextcloud-access.conf.avail
    dest: /etc/httpd/conf.d/z-nextcloud-access.conf
    state: link
  notify: verify config and restart httpd

- name: Setup the selinux boolean for proxying and GitHub auth
  seboolean:
    name: "{{ item }}"
    state: yes
    persistent: yes
  with_items:
    # for github auth and maybe others
    - httpd_can_network_connect
    # for postgresql
    - httpd_can_network_connect_db
