_______________________________________________
$display_name mailing list -- $listname
To unsubscribe send an email to ${short_listname}-leave@${domain}
Privacy Statement: https://www.redhat.com/en/about/privacy-policy
List Archives: ${hyperkitty_url}
